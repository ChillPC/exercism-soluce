(import (rnrs))

(define (pascals-row old)
  (define (pascals-row-alt old acc)
    (cond
      [(null? (cdr old)) acc]
      [else (pascals-row-alt
              (cdr old)
              (cons (+ (car old) (car (cdr old))) acc))]
      ))
  (cons 1 (pascals-row-alt old '(1))))

(define (pascals-triangle n)
  (define (pascals-triangle-alt n acc)
    (if (= n 0)
      acc
      (pascals-triangle-alt (- n 1) (cons (pascals-row (car acc)) acc))))
  (cond
    [(= n 0) '()]
    [(= n 1) '((1))]
    [(= n 2) '((1) (1 1))]
    [else (reverse (pascals-triangle-alt (- n 2) '((1 1) (1))))]))


(display (pascals-triangle 10)) (newline)
(display (pascals-triangle 3)) (newline)
(display (list? (car (pascals-triangle 10)))) (newline)
(exit)
