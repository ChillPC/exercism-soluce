(import (rnrs))

(define (dna->rna dna)
  (define (dna->rna-alt dna-list acc)
    (if (null? dna-list)
      acc
      (dna->rna-alt
        (cdr dna-list)
        (cons
          (let [(dna (car dna-list))]
            [cond
              [(eq? dna #\G) #\C]
              [(eq? dna #\C) #\G]
              [(eq? dna #\T) #\A]
              [(eq? dna #\A) #\U]
              [else dna]
              ])
          acc
          ))))
  (list->string (reverse (dna->rna-alt (string->list dna) '()))))
