(import (rnrs))


(define (hamming-distance strand-a strand-b)
  (fold-left + 0 (map (lambda (x y) (if (eq? x y)
                                      0
                                      1))
                      (string->list strand-a)
                      (string->list strand-b)
                      )))


