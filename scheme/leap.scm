(import (rnrs))

(define (divisible? i n)
  (= (modulo i n) 0))

(define (leap-year? year)
  (cond
    [(not (divisible? year 4)) #f]
    [(not (divisible? year 100)) #t]
    [(not (divisible? year 400)) #f]
    [else #t]
    ))

