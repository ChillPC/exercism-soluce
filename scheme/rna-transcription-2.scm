(import (rnrs))

(define (nucl-dna->rna dna)
  (cond
    [(eq? dna #\G) #\C]
    [(eq? dna #\C) #\G]
    [(eq? dna #\T) #\A]
    [(eq? dna #\A) #\U]
    [else dna]
    ))

(define (dna->rna dna)
  (list->string (map nucl-dna->rna (string->list dna))))
