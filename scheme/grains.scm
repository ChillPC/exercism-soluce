(import (rnrs))

(define (calc-tiles n)
  (define (calc-tiles-acc n acc)
    (if (= 0 n)
      (reverse acc)
      (calc-tiles-acc (- n 1) (cons (* (car acc) 2) acc))))
  (cond
    [(= 0 n) 0]
    [(= 1 n) 1]
    [else (calc-tiles-acc (- n 2) (list 2 1 0))]))

(define tiles
  (calc-tiles 64))

(define (square n)
  (cond
    [(= 0 n) (error #f "square 0 raises an exception")]
    [else (list-ref tiles n)])
  )


(define total
  (fold-left + 0 tiles))


;; ---------

;; (import (rnrs))

;; (define (square n)
;;   (define (square-acc n acc)
;;     (if (= 0 n)
;;       acc
;;       (square-acc (- n 1) (* acc 2))))
;;   (cond
;;     [(= 0 n) 0]
;;     [(= 1 n) 1]
;;     [else (square-acc (- n 2) 2)]))

;; (define total
;;   (square 64))
