(import (rnrs))

(define (anagram? target word)
  (let [(target-list (string->list (string-downcase target)))
        (word-list (string->list (string-downcase word)))]
    (and
      (not (equal? target-list word-list))
      (equal?
        (list-sort char<? target-list)
        (list-sort char<? word-list)
        ))))

(define (anagram target words)
  (filter (lambda (word) (anagram? target word)) words))
