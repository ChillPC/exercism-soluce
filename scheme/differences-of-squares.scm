(import (rnrs))

(define (square-of-sum n)
  (define (square-of-sum-alt n acc)
    (if (zero? n)
      acc
      (square-of-sum-alt (- n 1) (+ acc n))))
  (expt (square-of-sum-alt n 0) 2))

(define (sum-of-squares n)
  (define (sum-of-squares-alt n acc)
    (if (zero? n)
      acc
      (sum-of-squares-alt (- n 1) (+ acc (expt n 2)))))
  (sum-of-squares-alt n 0))

(define (difference-of-squares n)
  (- (square-of-sum n) (sum-of-squares n)))
