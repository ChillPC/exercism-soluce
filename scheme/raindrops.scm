(import (rnrs))

(define (convert number)
  (define (convert-alt number factor sound)
    (if (= (modulo number factor) 0)
      sound
      ""))
  (let [(res
          (apply string-append
                 (map
                   (lambda (x) (convert-alt number (car x) (cdr x)))
                   '((3 . "Pling") (5 . "Plang") (7 . "Plong")))))]
  (if (equal? res "")
    (number->string number)
    res)
  ))
